//
// Created by ASUS on 25.12.2021.
//

#ifndef INC_2_REDEFINEMAINWINDOW_H
#define INC_2_REDEFINEMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <iostream>

class RedefineMainWindow : public QMainWindow
{
Q_OBJECT

public:
    QPlainTextEdit *plainTextEdit = nullptr;
    QWebEngineView*webEngineView = nullptr;

    RedefineMainWindow(QWidget *parent = nullptr) : QMainWindow(parent) {}

public slots:
    void slotHTML()
    {
        //std::cout << "----------------------";
        webEngineView->setHtml(plainTextEdit->toPlainText());
    };
};

#endif //INC_2_REDEFINEMAINWINDOW_H
